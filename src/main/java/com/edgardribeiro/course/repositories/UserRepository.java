package com.edgardribeiro.course.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edgardribeiro.course.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{
	

}
